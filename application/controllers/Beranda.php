<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function index()
    {
        $this->load->library('turunan');
        $turunan = new Turunan();
        $turunan->setInterval(0.05);
        $turunan->setAwal(0);
        $turunan->setAkhir(1);

        $x = $turunan->getAwal();

        $data['interval'] = $turunan->getInterval();
        $data['awal'] = $turunan->getAwal();
        $data['akhir'] = $turunan->getAkhir();
        while (true) {
            if ($x > $turunan->getAkhir() + 0.05)
                break;
            $data['fx']["$x"] = $turunan->fungsiX($x);
            $data['maju']["$x"] = $turunan->selisihMaju($x);
            $data['mundur']["$x"] = $turunan->selisihMundur($x);
            $data['tengah']["$x"] = $turunan->selisihTengah($x);
            $data['eksak']["$x"] = $turunan->turunanX($x);
            $x += $turunan->getInterval();
        }
        $this->load->view('beranda', $data);
    }
}
