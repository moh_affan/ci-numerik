<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nonlinear extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function biseksi()
    {
        $this->load->library('non_linear',[],'biseksi');
        $this->biseksi->set_batas_bawah(-1);
        $this->biseksi->set_batas_atas(0);
        $this->biseksi->set_toleransi(0.001);
        $this->biseksi->set_max_iterasi(10);
        $data['hasil'] = $this->biseksi->hitung_biseksi();
        $this->load->view('biseksi', $data);
    }

    public function regulafalsi()
    {
        $this->load->library('non_linear');
        $this->non_linear->set_batas_bawah(-1);
        $this->non_linear->set_batas_atas(0);
        $this->non_linear->set_toleransi(0.001);
        $this->non_linear->set_max_iterasi(10);
        $data['hasil'] = $this->non_linear->hitung_regulafalsi();
        $this->load->view('regulafalsi', $data);
    }
}
