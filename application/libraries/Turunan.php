<?php

/**
 * Created by PhpStorm.
 * User: affanmohammad
 * Date: 12/18/16
 * Time: 08:31
 */

class Turunan
{
    private $interval;
    private $awal;
    private $akhir;

    public function setInterval($interval)
    {
        $this->interval = floatval($interval);
    }

    public function setAwal($awal)
    {
        $this->awal = floatval($awal);
    }

    public function setAkhir($akhir)
    {
        $this->akhir = floatval($akhir);
    }

    public function getAkhir()
    {
        return $this->akhir;
    }

    public function getAwal()
    {
        return $this->awal;
    }

    public function getInterval()
    {
        return $this->interval;
    }

    public function fungsiX($x)
    {
        return 2 * $x * $x;
    }

    public function turunanX($x)
    {
        return 4 * $x;
    }

    public function selisihMaju($x)
    {
        $value = null;
        $next_x = $x + $this->interval;
        if ($next_x < $this->akhir+$this->interval) {
            $value = ($this->fungsiX($x + $this->interval) - $this->fungsiX($x)) / $this->interval;
        }
        return $value;
    }

    public function selisihMundur($x)
    {
        $value = null;
        if ($x > $this->awal) {
            $value = ($this->fungsiX($x) - $this->fungsiX($x - $this->interval)) / $this->interval;
        }
        return $value;
    }

    public function selisihTengah($x)
    {
        $value = null;
        if ((($x + $this->interval) < $this->akhir+$this->interval) && ($x > $this->awal)) {
            $value = ($this->fungsiX($x + $this->interval) - $this->fungsiX($x - $this->interval)) / (2 * $this->interval);
        }
        return $value;
    }
}