<?php

/**
 * Created by PhpStorm.
 * User: USER
 * Date: 14/01/2017
 * Time: 3:19
 */
class non_linear
{
    private $batasBawah;
    private $batasAtas;
    private $toleransi;
    private $maxIterasi;
    private $hasil;

    public function set_batas_bawah($low)
    {
        $this->batasBawah = $low;
    }

    public function set_batas_atas($up)
    {
        $this->batasAtas = $up;
    }

    public function set_toleransi($e)
    {
        $this->toleransi = $e;
    }

    public function set_max_iterasi($max)
    {
        $this->maxIterasi = $max;
    }

    private function fx($x)
    {
        return $x * exp(-$x) + 1;
    }

    private function gx($x)
    {

    }

    public function hitung_biseksi()
    {
        $a = $this->batasBawah;
        $b = $this->batasAtas;
        if (($this->fx($a) * $this->fx($b)) > 0) {
            return false;
        }
        $c = 1;
        do {
            $x = ($a + $b) / 2;
            $this->hasil['hasil'][$c]['iterasi'] = $c;
            $this->hasil['hasil'][$c]['a'] = $a;
            $this->hasil['hasil'][$c]['b'] = $b;
            $this->hasil['hasil'][$c]['x'] = $x;
            $this->hasil['hasil'][$c]['fa'] = $this->fx($a);
            $this->hasil['hasil'][$c]['fx'] = $this->fx($x);
            if (($this->fx($x) * $this->fx($a)) < 0) {
                $b = $x;
            } else {
                $a = $x;
            }
            $c++;
        } while (abs($b - $a) > $this->toleransi || $c <= $this->maxIterasi);
        return json_decode(json_encode($this->hasil));
    }

    public function hitung_regulafalsi()
    {
        $this->hasil = [];
        $a = $this->batasBawah;
        $b = $this->batasAtas;
        $c = 1;
        do {
            $x = (($this->fx($b) * $a) - ($this->fx($a) * $b)) / ($this->fx($b) - $this->fx($a));
            $this->hasil['hasil'][$c]['iterasi'] = $c;
            $this->hasil['hasil'][$c]['a'] = $a;
            $this->hasil['hasil'][$c]['b'] = $b;
            $this->hasil['hasil'][$c]['x'] = $x;
            $this->hasil['hasil'][$c]['fx'] = $this->fx($x);
            $error = abs($this->fx($x));
            $this->hasil['hasil'][$c]['fa'] = $this->fx($a);
            $this->hasil['hasil'][$c]['fb'] = $this->fx($b);
            if (($this->fx($x) * $this->fx($a)) < 0) {
                $b = $x;
            } else {
                $a = $x;
            }
            $c++;
        } while ($c <= $this->maxIterasi || $error > $this->toleransi);
        return json_decode(json_encode($this->hasil));
    }
}
